variable "instance_id" {
  description = "instance id which is attched to elastic ip"
}
variable "elastic_ip_tag_value" {
  description = "elastic ip tag value"
}
