variable "instance_type" {
  description = "instance type for K8s creation Server"
}

variable "instance_ami" {
  description = "AMI for Instance"
}

variable "instance_key" {
  description = "Key for instance ssh"
}

variable "server_name" {
  description = "aws instance name"
}

variable "subnet_id" {
  description = "instance subnet id"
  type = ""
}

variable "security_group" {
  description = "instance security group"
  
}

variable "volume_type" {
  description = "instance volume type"
}

variable "volume_size" {
  description = "instance volume size"
}

variable "delete_on_termination" {
  description = "instance termination value"
}


