########## creating bastion host Instance ###########
resource "aws_instance" "ec2_server" {
  instance_type          = "${var.instance_type}"
  ami                    = "${var.instance_ami}"
  key_name               = "${var.instance_key}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group}"]

  root_block_device {
    volume_type           = "${var.volume_type}"
    volume_size           = "${var.volume_size}"
    delete_on_termination = "${var.delete_on_termination}"
    encrypted             = "true"
  }

  tags {
    Name = "${var.server_name}"
  }
}


