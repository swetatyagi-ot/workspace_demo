resource "aws_security_group" "pub_sg" {
  name        = "${var.security_group_name}"
  description = "${var.security_group_name} group managed by Terraform"

  vpc_id = "${var.vpc_id}"
  tags {
        Name = "${var.tag_name_value}"
    }
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "All egress traffic"
  security_group_id = "${aws_security_group.pub_sg.id}"
}

resource "aws_security_group_rule" "tcp" {
  count             = "${length(var.description)}"
  type              = "ingress"
  from_port         = "${element(split(",", var.tcp_ports), 0)}"
  to_port           = "${element(split(",", var.tcp_ports), 0)}"
  protocol          = "tcp"
  cidr_blocks       = ["${element(var.cidrs,count.index)}"]
  description       = "${element(var.description,count.index)}"
  security_group_id = "${aws_security_group.pub_sg.id}"
}