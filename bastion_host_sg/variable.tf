
variable "tag_name_value" {
    description = "The value of tag Name"
}

variable "tcp_ports" {
  default = "default_null"
}

variable "cidrs" {
  type = "list"
}

variable "security_group_name" {}

variable "vpc_id" {}

variable "description" {
  type = "list"
}
