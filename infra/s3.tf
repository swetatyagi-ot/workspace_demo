terraform {
  backend "s3" {
    bucket  = "tfstate"
    key     = "base-infra/state"
    region  = "us-west-2"
  }
}
