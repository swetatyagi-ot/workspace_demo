################# Region of Infra #################
provider "aws" {
    version                 = "~> 2.0"
    shared_credentials_file = "${var.key_credentials_path}"
    region                  = "${var.infra_region}"

}


######## Creating VPC ####################
module "vpc" {
    source               = "../vpc"
    cidr_block           = "${var.vpc_cidr_block}"
    instance_tenancy     = "${var.instanceTenancy}"
    enable_dns_support   = "${var.dnsSupport}"
    enable_dns_hostnames = "${var.dnsHostNames}"
    vpc_Name_tag         = "${var.env}_${var.vpc_tag_name}"
}

########Creating Internet Gateway################
module "internet_gateway" {
    source       = "../internat_gateway"
    vpc_id       = "${module.vpc.id}"
    ig_tag_Name  = "${var.env}_${var.igw_name}"
}

#########Creating First Public Subnet ##############
module "bastion_host_pub_subnet_az_a" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_pub_subnet_az_a}"
  availability_zone        = "${var.availability_zone_a}"
  subnet_name              = "${var.env}_${var.pub_subnet_name_az_a}"
}
############Creating Second Public Subnet############
module "bastion_host_pub_subnet_az_b" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_pub_subnet_az_b}"
  availability_zone        = "${var.availability_zone_b}"
  subnet_name              = "${var.env}_${var.pub_subnet_name_az_b}"
}

#####Creating route table for Public Subnets#########
module "pub_route_table" {
  source                  = "../route_table"
  vpc_id                  = "${module.vpc.id}"
  cidr_block_routetable   = "${var.cidr_block_pub_rt}"
  gateway_id              = "${module.internet_gateway.igw_id}"
  tag_Name                = "${var.env}_${var.rt_pub_tag_name}"
}

###########3Associate Public Route Table with jump server public subnets
module "routetable_associations_to_pub_subnet_az_a" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.bastion_host_pub_subnet_az_a.subnet_id}"
  routetable_id           = "${module.pub_route_table.routetable_id}"
}
module "routetable_associations_to_pub_subnet_az_b" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.bastion_host_pub_subnet_az_b.subnet_id}"
  routetable_id           = "${module.pub_route_table.routetable_id}"
}

   
######## Creating Security group for intstances
module "bastion_host_security_group" {
  source                  = "../bastion_host_sg"
  security_group_name     = "${var.env}_${var.bastion_host_security_group_name}"
  vpc_id                  = "${module.vpc.id}"
  tcp_ports               = "${var.bastion_host_ports}"
  cidrs                   = ["${var.cidrs}"]
  tag_name_value          = "${var.env}_${var.bastion_host_security_group_tag}"
  description             = "${var.bastion_host_sg_description}"
}

############## Creating bastion host##########
module "bastion_host" {
  source                  = "../bastion_host"
  instance_type           = "${var.instance_type}"
  instance_ami            = "${var.instance_ami}"
  instance_key            = "${var.key}"
  subnet_id               = "${module.bastion_host_pub_subnet_az_a.subnet_id}"
  security_group          = "${module.bastion_host_security_group.pub_sg_id}"
  volume_type             = "${var.volume_type}"
  volume_size             = "${var.volume_size}"
  delete_on_termination   = "${var.delete_on_termination}"
  server_name             = "${var.env}_${var.server_name}"
}

# ########### Creating elastic IP 
 module "elastic_ip" {
   source                  = "../elastic_ip"
   instance_id             = "${module.bastion_host.ec2_id}"
   elastic_ip_tag_value    = "${var.env}_${var.elastic_ip_tag_value}"
 }
