########## env name##############
variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default = ""
}

###### Variable for Region ##########
variable "infra_region" {
    description = "region for the infra"
    default = "us-west-2"
}
variable "key_credentials_path" {
    description = "key credentials path"
    default = ""
}

###################### VPC vars #####################
# VPC CIDR block
variable "vpc_cidr_block" {
    description = "The CIDR block for the VPC"
    default = "10.0.0.0/16"
}
# VPC Tenancy
variable "instanceTenancy" {
 default = "default"
}
# VPC dns support value
variable "dnsSupport" {
 default = true
}
# VPC dns host name value
variable "dnsHostNames" {
        default = true
}

###########Tag for VPC
variable "vpc_tag_name" {
    description = "Tag for VPC Name fields"
    default = "vpc"
}


################# INTERNET GATEWAY vars ##################
##### Integrnet Gateway Name
variable "igw_name" {
    description = "Name for Internet gateway"
    default = "igw"
}


###########variables for first public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_a" {
    description = "The CIDR block for the subnet."
    default = "10.0.15.0/24"
}
#### Tag Name 
variable "pub_subnet_name_az_a" {
    description = "name of public subnets"
    default = "pub_subnet_az_a"
}

###########variables for second public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_b" {
    description = "The CIDR block for the subnet."
    default = "10.0.16.0/24"
}
#### Tag Name 
variable "pub_subnet_name_az_b" {
    description = "name of public subnets"
    default = "pub_subnet_az_b"
}


####### Two Availability zone for all subnets#########3
variable "availability_zone_a" {
    description = "The AZ for the subnet"
    default = "us-west-2a"
} 
variable "availability_zone_b" {
    description = "The AZ for the subnet"
    default = "us-west-2b"
}

############# Variables for Public Route Table#############

### Public RT Name
variable "rt_pub_tag_name" {
  default = "pub_rt"
}
### CIDR Block
variable "cidr_block_pub_rt" {
  default = "0.0.0.0/0"
}

######## Variable for bastion host Security group ##############
### bastion host SG name
variable "bastion_host_security_group_name" {
  default = "bastion_host_sg"
}
###bastion host SG tag name
variable "bastion_host_security_group_tag" {
  default = "bastion_host_sg"
}
### bastion host SG CIDR 
variable "cidrs" {
  type = "list"
  default = ["0.0.0.0/0"]
}

###########bastion host sg description
variable "bastion_host_sg_description" {
  type = "list"
  default = ["opstree"]
}

#### SSH port value
variable "bastion_host_ports" {
  default = "22"
}

###########Variable for bastion host###########
######## variable for key name #########
variable "key" {
  description = "Enter Key name"
  default = ""
}

######## Variable for bastion host ami #########
variable "instance_ami" { 
default = ""
}
###  Instance type
variable "instance_type" {
  default = "t2.micro"
}
### instance volume type
variable "volume_type" {
  description = "instance volume type"
  default = "gp2"
}
### Instance Volume size
variable "volume_size" {
  description = "instance volume size"
  default = "50"
}

### instance termination value
variable "delete_on_termination" {
  description = "instance delete on termination"
  default = "true"
}

### Server name
variable "server_name" {
  description = "jump Server Name"
  default = "bastion_host"
}

######### Elastic Ip variable #######
variable "elastic_ip_tag_value" {
  default = "bastion_host_eip"
  description = "elastic ip name"
  
}
